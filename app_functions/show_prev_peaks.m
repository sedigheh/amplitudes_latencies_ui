function show_prev_peaks(fig,files_roi)

    hold(fig, 'on')
    colorpalette = ['c', 'r', 'g', 'b', 'm'];
    nwaves = 5;
    data_tmp = load(files_roi.name);
    
    for i = 1:nwaves
        if ~isempty(data_tmp.save_roi{i,1})
            pic_tmp.Position = data_tmp.save_roi{i,1};
            proi = drawpoint(fig,'Position',pic_tmp.Position, 'Color', colorpalette(i), 'Label', ['P' num2str(i)], 'Tag', ['P' num2str(i)],'LabelAlpha', 0.2);
            addlistener(proi,'ROIMoved',@allevents);
        end
        if ~isempty(data_tmp.save_roi{i,2})
            node_tmp.Position = data_tmp.save_roi{i,2};
            nroi = drawpoint(fig,'Position',node_tmp.Position, 'Color', colorpalette(i), 'Label', ['N' num2str(i)], 'Tag', ['N' num2str(i)],'LabelAlpha', 0.2);
            addlistener(nroi,'ROIMoved',@allevents);
        end
    end


    function allevents(~,evt)
        disp(['ROI moved Previous Position: ' mat2str(evt.PreviousPosition)]);
        disp(['ROI moved Current Position: ' mat2str(evt.CurrentPosition)]);
    end


end