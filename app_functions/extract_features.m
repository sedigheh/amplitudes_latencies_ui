function [table_results, sTable] = extract_features(method_amp, dat, Filename, system, plotfig, dBlevel, table_results,fig,table_window)

nwaves = 5;
amp_peak_all     = nan(nwaves,1); 
amp_node_all     = nan(nwaves,1); 
latency_peak_all = nan(nwaves,1); 
latency_node_all = nan(nwaves,1);
save_roi = cell(nwaves,2);

for i = 1:nwaves
    node_tmp = findobj(plotfig,'Tag',['N' num2str(i)]);
    pic_tmp  = findobj(plotfig,'Tag',['P' num2str(i)]);
    if ~isempty(pic_tmp)
        save_roi{i,1}       = pic_tmp.Position;
        amp_peak_all(i)     = pic_tmp.Position(2);
        latency_peak_all(i) = pic_tmp.Position(1);
    end
    if ~isempty(node_tmp)
        save_roi{i,2}       = node_tmp.Position;
        amp_node_all(i)     = node_tmp.Position(2);
        latency_node_all(i) = node_tmp.Position(1);
    end

end

switch method_amp
    case 1 % default N1 P1
        amplitudeI   = distance_vert(amp_peak_all(1), amp_node_all(1));
        amplitudeII  = distance_vert(amp_peak_all(2), amp_node_all(2));
        amplitudeIII = distance_vert(amp_peak_all(3), amp_node_all(3));
        amplitudeIV  = distance_vert(amp_peak_all(4), amp_node_all(4));
        amplitudeV   = distance_vert(amp_peak_all(5), amp_node_all(5));
        
        cumulative_amp = 0; 
        for i=1:5
            cumulative_amp = cumulative_amp + distance_vert(amp_peak_all(i), amp_node_all(i));
        end

    case 2 % P1 to N2
        amplitudeI   = distance_vert(amp_peak_all(1), amp_node_all(2));
        amplitudeII  = distance_vert(amp_peak_all(2), amp_node_all(3));
        amplitudeIII = distance_vert(amp_peak_all(3), amp_node_all(4));
        amplitudeIV  = distance_vert(amp_peak_all(4), amp_node_all(5));
        amplitudeV = 0;% pas d'amplitude V
        cumulative_amp = 0; 
        for i=1:4
            cumulative_amp = cumulative_amp + distance_vert(amp_peak_all(i), amp_node_all(i+1));
        end

end


%get sound level parameters
%noise levels
try Lmin_nois = dat.stimparams.Lmin_nois; 
    Lmax_nois = dat.stimparams.Lmax_nois;
    dL_nois   = dat.stimparams.delL_nois; 
catch 
    Lmin_nois = 0;
    Lmax_nois = 0;
    dL_nois = 0;
end
if(dL_nois==0) 
    Lnois=Lmin_nois;
elseif(dL_nois>0)
    Lnois=Lmin_nois:dL_nois:Lmax_nois;
    Lnois = [0 Lnois];
elseif(dL_nois<0)
    Lnois=Lmax_nois:dL_nois:Lmin_nois;
    Lnois = [0 Lnois];
end

Lmin = dat.stimparams.Lmin;
Lmax = dat.stimparams.Lmax;
dL = dat.stimparams.delL;

if dL==0
    L = Lmin.*ones(1,length(Lnois));
elseif(dL>0)
    L = Lmin:dL:Lmax;
    Lnois=Lmin_nois*ones(1,length(L));
elseif(dL<0)
    L=Lmax:dL:Lmin;
    Lnois=Lmin_nois*ones(1,length(L));
end

if strcmp(system,'CERIAH')
    L = 1:size(dat.hrav,2);
    L(1:8)=[110 110 80 80 8085 9085 10085 11085];
    % 1- 110dB PeSPL R / L no noise
    % 2- 110dB PeSPL R / L no noise
    % 3- 80dB PeSPL R  / L no noise
    % 4- 80dB PeSPL R  / L no noise
    % 5- 80dB PeSPL R  / 85dB PeSPL
    % 6- 90dB PeSPL R  / 85dB PeSPL
    % 7- 100dB PeSPL R / 85dB PeSPL
    % 8- 110dB PeSPL R / 85dB PeSPL
elseif strcmp(system,'Otophylab')
    L=dat.stimparams.L;
end

Ltmp = L(dBlevel);
Mtmp = [Ltmp, amplitudeI,latency_node_all(1),latency_peak_all(1), amplitudeII,latency_node_all(2),latency_peak_all(2), amplitudeIII,latency_node_all(3),latency_peak_all(3), amplitudeIV,latency_node_all(4),latency_peak_all(4), amplitudeV,latency_node_all(5),latency_peak_all(5), cumulative_amp];

table_results(dBlevel, :) = Mtmp;
colNames = {'dB', 'amplitudeI','latencyNI','latencyPI','amplitudeII','latencyNII','latencyPII','amplitudeIII','latencyNIII','latencyPIII','amplitudeIV','latencyNIV','latencyPIV','amplitudeV','latencyNV','latencyPV','cumulative_amp'};
sTable = array2table(table_results,'VariableNames',colNames);
uit = uitable(fig,'Data',sTable);
uit.Position = table_window.Position; %[35,31,574,185];

save([Filename(1:end-4) filesep 'roi_coordinate_dBlevel_' num2str(dBlevel)], 'save_roi')

end
