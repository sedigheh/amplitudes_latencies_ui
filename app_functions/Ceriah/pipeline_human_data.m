%%% Main script to prepare the CERIAH files to be read by the Amplitude &
%%% Latencies Matlab User Interface.
%%% It requires the read_data function also provided in the Gitlab of the PF. 
%%% Clara Dussaux, PF Acquisition et Traitement du Signal IDA.


clear; close all; clc; 

%%%%% Protocol is 
% 1- 110dB PeSPL R / L no noise
% 2- 110dB PeSPL R / L no noise
% 3- 80dB PeSPL R  / L no noise
% 4- 80dB PeSPL R  / L no noise
% 5- 80dB PeSPL R  / 85dB PeSPL
% 6- 90dB PeSPL R  / 85dB PeSPL
% 7- 100dB PeSPL R / 85dB PeSPL
% 8- 110dB PeSPL R / 85dB PeSPL

selpath = uigetdir(); 
cd(selpath)
Files=dir('*.txt');
timedata=[];
h = [];
folder = pwd;
subject = folder(end);

for k=1:length(Files)
   FileNames=Files(k).name;
   [timedata(k,:), h(k,:)] = read_data(FileNames);
end

nplots = length(Files);
figure, set(gcf,'position',[125 50 400 72*nplots])
hold on
for k=1:nplots
    plot(timedata(k,:), h(k,:)-k)
    text(timedata(k,end),h(k,end)-k,['File' num2str(k)],'fontsize',11)
end

xlabel('t (ms)')
title(['Subject number ' subject])

stimparams.system = 'CERIAH'; 
stimparams.Ltube = '0';
stimparams.fech = 32; %32kHz
stimparams.gain = 1e6; %because it is already included in the read_data so I just reput them in V.
stimparams.espace = 2*2; % onset is at 2ms 
stimparams.Lmin=0;
stimparams.Lmax=0;
stimparams.delL=0;
stimparams.fmin=0; % clic
stimparams.fmax=0;
stimparams.delf=0;

hrav = h';
tt1 = timedata(1,:)*1e-3;
save('data_filter', 'tt1','hrav','stimparams');
savefig('ABR_data_filter2');