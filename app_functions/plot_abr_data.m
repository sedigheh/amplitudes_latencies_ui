function plot_abr_data(dat, computerId,fname,pltyp,gain,step,reverse)
%plot of abr data in standard (juxtaposition of waveforms) or browser form 

hsgn = reverse; %1 is normal, -1 is in the animal facility

Lmin = dat.stimparams.Lmin; 
Lmax = dat.stimparams.Lmax; 
dL   = dat.stimparams.delL;

if(dL==0) 
    L = Lmin;
elseif(dL>0) 
    L = Lmin:dL:Lmax; 
elseif(dL<0) 
    L = Lmax:dL:Lmin; 
end

fmin = dat.stimparams.fmin; 
fmax = dat.stimparams.fmax; 
if(isfield(dat.stimparams,'delf')) 
    df = dat.stimparams.delf;
else 
    df = (fmax-fmin); 
end
if(df==0)
    f = fmin;
elseif(df>0)
    f = fmin:df:fmax;
elseif(df<0)
    f = fmax:df:fmin;
end
    
e = dat.stimparams.espace;
%rescale abr waveform to �V units
hrfac = 1e6./gain; 
try 
    hrav = hsgn*hrfac.*dat.hrav;
    version=1;
catch
    hrav = hsgn*hrfac.*dat.hravsing;
    version=2; 
end

tt1 = dat.tt1;

if strcmp(computerId,'ABR1')
    minhrge=hrfac./4;
elseif strcmp(computerId,'ABR2')
    minhrge=hrfac./20;
else 
    minhrge = 5; % Check for TDT  
end

disp(['waveform datasize ' num2str(size(hrav))])
hrge = max(hrav(:))-min(hrav(:)); 
%stretchfac=0.8;
disp(['peak-peak amplitude: ' num2str(hrge)]);
disp(['minhrge: ' num2str(minhrge)]);

colorpalette = ["#0072BD", "#D95319", "#EDB120", "#7E2F8E", "#77AC30", "#4DBEEE", "#A2142F", "#0072BD", "#D95319", "#EDB120", "#7E2F8E"];
LdB_ref = 110:-10:10;

if strcmp(computerId,'CERIAH')
    L=1:size(hrav,2);
elseif strcmp(computerId,'Otophylab')
    L=dat.stimparams.L;
end


switch pltyp

    case {1,'standard_prev'}% previous standard juxtaposed ABR waveform plot
        
        %plotlevel=0; %hrge=max(minhrge,stretchfac.*hrge);
        if(length(L)>1) 
            dL=(L(end)-L(1))./(length(L)-1);
        else 
            dL=0;
        end       
        naplot=1:size(hrav,2);
        nfplot=1:size(hrav,3);
        nplots=max(size(hrav,2),size(hrav,3));
        for nf=nfplot
            if version==1
                f0 = f(nf);
            else
                f0 = abs(str2double(fname(end-12:end-11)));
            end
            if isnan(f0) 
               f0 = f(nf); % old way to svae data
            end
            plotlevel=0;
            hrge=max(minhrge,max(hrav(:,1,nf))-min(hrav(:,1,nf))); %disp(num2str([hrge]))
            for na=naplot
                % plot accumulated data in a separate figure
                mhrav=mean(hrav(:,na,nf));
                if(na==1)%&&nf==1)
                    figure, set(gcf,'position',[125 50 400 72*nplots])
                    if strcmp(computerId,'CERIAH')
                        plot(1000*tt1-e/2,hrav(:,na,nf)+plotlevel,'buttondownfcn',{@Mouse_Callback2,'down'}), nfig=gcf; hold on
                    else
                        try
                            plot(1000*tt1-e/2,hrav(:,na,nf)+plotlevel, 'Color', colorpalette{LdB_ref == L(na)},'buttondownfcn',{@Mouse_Callback2,'down'})
                        catch
                            plot(1000*tt1-e/2,hrav(:,na,nf)+plotlevel,'buttondownfcn',{@Mouse_Callback2,'down'})
                        end
                        
                        nfig=gcf; hold on
                        text(1010*tt1(end)-e/2,plotlevel+hrav(end,na,nf),[num2str(L(na)) 'dB'],'fontsize',11,'Color', colorpalette{LdB_ref == L(na)})
                    end
                    plot([0 0],mhrav+plotlevel+[-1.5*hrge 1.5*hrge],'--k')
                    xlabel('t (ms)')
                    title(['f = ' num2str(f0) 'kHz, L = ' num2str(L(1)) '-' num2str(L(end)) 'dB'])
                    %title(['f = ' num2str(f(nf)) 'kHz, L = ' num2str(L(1)) '-' num2str(L(end)) 'dB'])
                else 
                    figure(nfig)
                    if strcmp(computerId,'CERIAH')
                        hold on, plot(1000*tt1-e/2,hrav(:,na,nf)'+plotlevel,'buttondownfcn',{@Mouse_Callback2,'down'})
                    else
                        hold on,
                        try
                            plot(1000*tt1-e/2,hrav(:,na,nf)'+plotlevel, 'Color', colorpalette{LdB_ref == L(na)},'buttondownfcn',{@Mouse_Callback2,'down'})
                            text(1010*tt1(end)-e/2,plotlevel+hrav(end,na,nf),[num2str(L(na)) 'dB'],'fontsize',11, 'Color', colorpalette{LdB_ref == L(na)})
                      catch
                            plot(1000*tt1-e/2,hrav(:,na,nf)'+plotlevel, 'buttondownfcn',{@Mouse_Callback2,'down'})
                            text(1010*tt1(end)-e/2,plotlevel+hrav(end,na,nf),[num2str(L(na)) 'dB'],'fontsize',11)
                        end
                    end
                    plot([0 0],mhrav+plotlevel+[-1.5*hrge 1.5*hrge],'--k')    
                end               
                plotlevel=plotlevel+sign(dL+df).*hrge;
            end
            if any(strcmp(computerId,{'ABR1', 'ABR2'}))
                set(gca,'xlim',[-10 16])%[-15 20]
            end
            saveas(figure(nfig),[fname(1:end-13) num2str(f0) 'kHz.jpeg'])
            %title(['f = ' num2str(f(nf)) 'kHz, L = ' num2str(L(1)) '-' num2str(L(end)) 'dB'])
        end
        
    case {2,'browser'}
        
        tmark=[0,10];
        if(length(L)>1)
           for k=1:length(L) 
               mark{k}=[num2str(L(k)) ' dB']; 
           end
        else
           for k=1:length(f)
               mark{k}=[num2str(f(k)) ' kHz'];
           end            
        end
        set(gcf,'position',[530 50 550 450])
        plot_browser_abr(squeeze(hrav),1000*tt1'-e/2,tmark,mark);

    case {3,'standard'}
        
        if(length(L)>1) 
            dL=(L(end)-L(1))./(length(L)-1);
        else 
            dL=0;
        end       
        naplot=1:size(hrav,2);
        nfplot=1:size(hrav,3);
        nplots=max(size(hrav,2),size(hrav,3));
        for nf=nfplot
            if version==1
                f0 = f(nf);
            else
                f0 = abs(str2double(fname(end-12:end-11)));
            end
            if isnan(f0) 
               f0 = f(nf); % old way to svae data
            end
            
            plotlevel=0;
            hrge=step; % fixed step for easier comparison between differents acquisitions 
            for na=naplot
                % plot accumulated data in a separate figure
                mhrav=mean(hrav(:,na,nf));
                if(na==1)%&&nf==1)
                    figure, set(gcf,'position',[125 50 400 72*nplots])
                    if strcmp(computerId,'CERIAH')
                        plot(1000*tt1-e/2,hrav(:,na,nf)+plotlevel,'buttondownfcn',{@Mouse_Callback2,'down'}), nfig=gcf; hold on
                        text(1010*tt1(end)-e/2,plotlevel+hrav(end,na,nf),['File' num2str(L(na))],'fontsize',11,'Color', colorpalette{LdB_ref == L(na)})
                    else
                        try
                            plot(1000*tt1-e/2,hrav(:,na,nf)+plotlevel, 'Color', colorpalette{LdB_ref == L(na)},'buttondownfcn',{@Mouse_Callback2,'down'})
                        catch
                            plot(1000*tt1-e/2,hrav(:,na,nf)+plotlevel,'buttondownfcn',{@Mouse_Callback2,'down'})
                        end
                        nfig=gcf; hold on
                        text(1010*tt1(end)-e/2,plotlevel+hrav(end,na,nf),[num2str(L(na)) 'dB'],'fontsize',11,'Color', colorpalette{LdB_ref == L(na)})
                    end
                    plot([0 0],mhrav+plotlevel+[-1.5*hrge 1.5*hrge],'--k')
                    xlabel('t (ms)')
                    title(['f = ' num2str(f0) 'kHz, L = ' num2str(L(1)) '-' num2str(L(end)) 'dB'])
                    %title(['f = ' num2str(f(nf)) 'kHz, L = ' num2str(L(1)) '-' num2str(L(end)) 'dB'])
                else 
                    if strcmp(computerId,'CERIAH')
                        hold on, plot(1000*tt1-e/2,hrav(:,na,nf)'+plotlevel,'buttondownfcn',{@Mouse_Callback2,'down'})
                        text(1010*tt1(end)-e/2,plotlevel+hrav(end,na,nf),['File' num2str(L(na))],'fontsize',11,'Color', colorpalette{LdB_ref == L(na)})
                    else
                        hold on
                        try
                            plot(1000*tt1-e/2,hrav(:,na,nf)'+plotlevel, 'Color', colorpalette{LdB_ref == L(na)},'buttondownfcn',{@Mouse_Callback2,'down'})
                        catch
                            plot(1000*tt1-e/2,hrav(:,na,nf)'+plotlevel,'buttondownfcn',{@Mouse_Callback2,'down'})
                        end
                        text(1010*tt1(end)-e/2,plotlevel+hrav(end,na,nf),[num2str(L(na)) 'dB'],'fontsize',11,'Color', colorpalette{LdB_ref == L(na)})
                    end
                    plot([0 0],mhrav+plotlevel+[-1.5*hrge 1.5*hrge],'--k')    
                end               
                plotlevel=plotlevel+sign(dL+df).*hrge;
            end
            if any(strcmp(computerId,{'ABR1', 'ABR2'}))
                set(gca,'xlim',[-15 20])%[-10 16]
            end
            saveas(figure(nfig),[fname(1:end-13) num2str(f0) 'kHz.jpeg'])
        end
        
end

